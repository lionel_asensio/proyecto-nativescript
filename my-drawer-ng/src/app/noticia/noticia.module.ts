import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NoticiaRoutingModule } from "./noticia-routing.module";
import { NoticiaComponent } from "./noticia.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NoticiaRoutingModule
    ],
    declarations: [
        NoticiaComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
