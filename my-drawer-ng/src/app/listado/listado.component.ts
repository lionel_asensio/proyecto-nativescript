import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { RouterExtensions } from "nativescript-angular/router";
import { isAndroid } from "tns-core-modules/ui/page/page";

@Component({
    selector: "Listado",
    templateUrl: "./listado.component.html"
})
export class ListadoComponent implements OnInit {

    listado = [];
    isAndroid: boolean;

    constructor(public noticiasService: NoticiasService, public routerExtensions: RouterExtensions) {

        if (app.android) {
            this.isAndroid = true;
        }
    
        this.listado = this.noticiasService.buscar();
   }

    ngOnInit(): void {
        
        if (this.isAndroid) {
            this.noticiasService.agregar("Android! (Clickeame)");
            this.noticiasService.agregar("Android!! (Clickeame)");
            this.noticiasService.agregar("Android!!! (Clickeame)");
        } else {
            this.noticiasService.agregar("Other!");
            this.noticiasService.agregar("Other!!");
            this.noticiasService.agregar("Other!!!");
        }
        
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        console.log(navItemRoute);
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

}
