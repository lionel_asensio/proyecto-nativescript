import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ListadoComponent } from "./listado.component";
import { NoticiaComponent } from "../noticia/noticia.component";

const routes: Routes = [
    { path: "", component: ListadoComponent },
    { path: "noticia", component: NoticiaComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ListadoRoutingModule { }
