import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ListadoRoutingModule } from "./listado-routing.module";
import { ListadoComponent } from "./listado.component";
import { NoticiaComponent } from "../noticia/noticia.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ListadoRoutingModule
    ],
    declarations: [
        ListadoComponent,
        NoticiaComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ListadoModule { }
